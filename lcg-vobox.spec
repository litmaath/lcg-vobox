%define topdir %(pwd)/rpmbuild
%define _topdir %{topdir}
Summary: VOBOX proxy renewal and agent service
Name: lcg-vobox
Version: 2.1.7
Vendor: LCG
Release: 1%{dist}
License: ASL 2.0
Group: LCG
Source: %{name}.src.tgz
BuildArch: noarch
Prefix: /
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Packager: LCG
Requires: initscripts

%description
VOBOX proxy renewal and agent service

%prep

%setup -c

%build
make install prefix=%{buildroot}%{prefix}

%files
%defattr(-,root,root,755)
%dir %{prefix}/etc/vobox
%dir %{prefix}/etc/vobox/templates
%{prefix}/etc/vobox/templates/voname-box-proxyrenewal
%{prefix}/usr/bin/vobox-proxy
%{prefix}/usr/sbin/vobox-renewd
%{prefix}/usr/share/man/man1/vobox-proxy.1
%{prefix}/usr/share/man/man8/vobox-renewd.8
%{prefix}/usr/share/man/man8/voname-box-proxyrenewal.8
%dir %{prefix}/var/lib/vobox

%clean
rm -rf %{buildroot}
