package=lcg-vobox
####################################################################
# Distribution Makefile
####################################################################

.PHONY: configure install clean

all: configure

####################################################################
# Prepare
####################################################################

prepare:
	mkdir -p build

####################################################################
# Configure
####################################################################

configure:
	@echo "No configuration required, use either 'make install' or 'make rpm'."

####################################################################
# Compile
####################################################################

compile:
	@echo "No compiling required, use either 'make install' or 'make rpm'."

####################################################################
# Install
####################################################################

TEMPL_DIR = $(prefix)/etc/vobox/templates
BIN_DIR   = $(prefix)/usr/bin
ETC_DIR   = $(prefix)/usr/etc
SBIN_DIR  = $(prefix)/usr/sbin
MAN1_DIR  = $(prefix)/usr/share/man/man1
MAN8_DIR  = $(prefix)/usr/share/man/man8
VAR_DIR   = $(prefix)/var/lib/vobox

install:
	@echo installing ...
	@mkdir -p $(TEMPL_DIR)
	@mkdir -p $(BIN_DIR)
	@mkdir -p $(ETC_DIR)
	@mkdir -p $(SBIN_DIR)
	@mkdir -p $(MAN1_DIR)
	@mkdir -p $(MAN8_DIR)
	@mkdir -p $(VAR_DIR)
	@install -m 0644 man/vobox-proxy.1             $(MAN1_DIR)
	@install -m 0644 man/vobox-renewd.8            $(MAN8_DIR)
	@install -m 0644 man/voname-box-proxyrenewal.8 $(MAN8_DIR)
	@install -m 0755 src/vobox-proxy               $(BIN_DIR)
	@install -m 0755 src/vobox-renewd              $(SBIN_DIR)
	@install -m 0755 src/voname-box-proxyrenewal   $(TEMPL_DIR)

####################################################################
# Build Distribution
####################################################################
rpm:
	@mkdir -p rpmbuild/RPMS/noarch
	@mkdir -p rpmbuild/SRPMS/
	@mkdir -p rpmbuild/SPECS/
	@mkdir -p rpmbuild/SOURCES/
	@mkdir -p rpmbuild/BUILD/
	@tar --gzip -cf rpmbuild/SOURCES/$(package).src.tgz \
	    Makefile lcg-vobox.spec man/vo* src/vo*
	@rpmbuild -ba $(package).spec

clean::
	rm -f $(PACKAGE_NAME).src.tgz
	rm -rf build
