#!/bin/bash
# vobox-renewd
# versions <= 1.0.1 by Simone Campana
# versions -> 1.0.4 by Elisa Lanciotti
# versions -> 1.0.5 by Andrew Elwell
# versions >= 2.0.0 by Maarten Litmaath
#
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

prog=`basename $0`

usage()
{
    echo "Usage: $prog VO-name VO-admin VOBOX-rootdir [command]" >&2
    echo "Commands: start | status | stop | restart" >&2
    exit 1
}

[ $# = 3 ] || [ $# = 4 ] || usage

for i
do
    case $i in
    -*)
	usage
    esac
done

vo=$1
voadmin=$2
rootdir=$3
cmd=${4:-start}

case $vo in
*[!-_.A-Za-z0-9]*)
    echo "$prog: illegal character in VO name '$vo'" >&2
    exit 1
esac

case $voadmin in
-*)
    echo "$prog: illegal character in VO admin name '$voadmin'" >&2
    exit 1
esac

case `id -nu` in
root)
    case $voadmin in
    root)
	echo "$prog: ERROR: the VO admin cannot be root" >&2
	exit 1
    esac

    exec su - "$voadmin" -c "$0 $*"
esac

(cd "$rootdir") || exit 1

#############################################################################

my_lock()
{
    (
	if [ $# != 3 ]
	then
	    echo "$0: ERROR: my_lock function needs 3 arguments, got $#" >&2
	    exit 1
	fi

	lock=$1
	pid=$2
	dir=$3

	kill -0 "$pid" || exit 1

	f=$dir/$pid

	echo "$pid" > "$f" || exit 1

	key=$dir/key-`date +%y%m%d%H%M%S`

	if ln "$f" "$key" 2> /dev/null
	then
	    echo "$key" >> "$f"
	else
	    #
	    # assume someone else beat us
	    #

	    rm -- "$f"
	    exit 1
	fi

	err=`ln "$key" "$lock" 2>&1`

	if [ "X$err" = X ]
	then
	    #
	    # we have the lock!
	    #

	    echo "$pid"
	    rm -- "$f"
	    exit
	fi

	if [ ! -e "$lock" ]
	then
	    echo "$err" >&2
	    rm -- "$f" "$key"
	    exit 1
	fi

	read owner junk 2> /dev/null < "$lock"

	case $owner in
	[1-9]*[0-9])
	    if [ "X$owner" = "X$pid" ]
	    then
		echo "$owner"
		rm -- "$f" "$key"
		exit
	    fi

	    if kill -0 "$owner" 2> /dev/null
	    then
		echo "$owner"
		rm -- "$f" "$key"
		exit 1
	    fi
	esac

	#
	# the oldest active key wins
	# note: dotted extensions are used by my_xfer!
	#

	for k in `ls "$dir" | egrep '^key-[0-9]{12}(\.[0-9]+)?$'`
	do
	    if [ "X$k" = "X${key##*/}" ]
	    then
		#
		# we are the winner!
		#

		mv "$f" "$lock" || exit 1

		echo "$pid"
		exit 0
	    fi

	    read owner junk 2> /dev/null < "$dir/$k"

	    case $owner in
	    [1-9]*[0-9])
		if kill -0 "$owner" 2> /dev/null
		then
		    echo "$owner"
		    rm -- "$f" "$key"
		    exit 1
		fi
	    esac

	    rm -- "$dir/$k" 2> /dev/null
	done

	echo "$0: ERROR: could not find key in directory listing" >&2
	echo "$0: ERROR: key: $key" >&2
	exit 1
    )
}

my_xfer()
{
    (
	if [ $# != 4 ]
	then
	    echo "$0: ERROR: my_xfer function requires 4 arguments, got $#" >&2
	    exit 1
	fi

	lock=$1
	pid=$2
	dir=$3
	pid2=$4

	if [ -e "$lock" ]
	then
	    (
		read owner junk
		read key junk

		if [ "X$owner" = "X$pid" ]
		then
		    if [ "X$key" = X ]
		    then
			echo "$0: ERROR: original key not found" >&2
			exit 1
		    fi

		    kill -0 "$pid2" || exit

		    tmp=$lock.$pid2
		    key2d=${key%/*}
		    key2f=${key##*/}
		    key2=$key2d/${key2f%.*}.$pid2

		    (echo "$pid2"; echo "$key2") > "$key2" || exit

		    ln "$key2" "$tmp" || exit

		    mv "$tmp" "$lock" && rm -- "$key"

		    exit
		fi

		[ "X$owner" = X ] && owner=unknown

		echo "$0: ERROR: lock held by another process: $owner" >&2
		exit 1

	    ) < "$lock"

	    exit
	fi

	echo "$0: ERROR: lock did not exist" >&2
	exit 1
    )
}

my_unlock()
{
    (
	if [ $# != 2 ]
	then
	    echo "$0: ERROR: my_unlock function requires 2 arguments, got $#" >&2
	    exit 1
	fi

	lock=$1
	pid=$2

	if [ -e "$lock" ]
	then
	    (
		read owner junk
		read key junk

		if [ "X$owner" = "X$pid" ]
		then
		    rm -- "$lock" "$key"
		    exit
		fi

		[ "X$owner" = X ] && owner=unknown

		echo "$0: ERROR: lock held by another process: $owner" >&2
		exit 1

	    ) < "$lock"

	    exit
	fi

	echo "$0: ERROR: lock did not exist" >&2
	exit 1
    )
}

my_status()
{
    (
	if [ $# != 1 ]
	then
	    echo "$0: ERROR: my_status function requires 1 argument, got $#" >&2
	    exit 1
	fi

	lock=$1

	if [ -e "$lock" ]
	then
	    read owner junk 2> /dev/null < "$lock"

	    case $owner in
	    [1-9]*[0-9])
		echo "$owner"
		kill -0 "$owner" 2> /dev/null
		exit
	    esac
	fi

	exit 1
    )
}

#############################################################################

vodir=$rootdir/$vo
lockdir=$vodir/lock
lock=$lockdir/lock
logdir=$vodir/log
proxy_repo=$vodir/proxy_repository
client=vobox-proxy

for d in $lockdir $logdir $proxy_repo
do
    mkdir -p  $d || exit 1
    chmod 750 $d || exit 1
done

stop()
{
    pid=`my_status $lock`
    val=$?

    if [ $val = 0 ]
    then
	my_xfer $lock "$pid" $lockdir $$
	xfer=$?

	kill -9 "$pid" && echo "$prog stopped" || val=1

	[ $xfer = 0 ] && my_unlock $lock $$ || val=1
    else
	echo "$prog not found running"
    fi

    return $val
}

case $cmd in
start)
    : # handled below
    ;;
status)
    pid=`my_status $lock`
    val=$?

    if [ $val = 0 ]
    then
	echo "Service running in pid: $pid"
    else
	echo "Service not running"
    fi

    exit $val
    ;;
stop)
    stop
    exit
    ;;
restart)
    stop
    # continue with start below
    ;;
*)
    usage
esac

f=/etc/profile.d/grid-env.sh
[ -r $f ] && . $f

pid=`my_lock $lock $$ $lockdir`
val=$?

if [ $val != 0 ]
then
    if [ "X$pid" = X ]
    then
	echo "$prog: could not obtain lock, bailing out!" >&2
    else
	echo "$prog: already running with PID $pid" >&2
    fi

    exit $val
fi

(
    log()
    {
	echo `date +'%Y/%m/%d %T'` "$@"
    }

    log "Starting"

    timeout=3600

    while :
    do
	#
	# reopen the logfile, as it may have been rotated
	#

        exec >> $logdir/events.log 2>&1 < /dev/null

	log "--------------------------------------------------"

	for proxy in `$client -vo $vo query-proxy-filename -dn all`
	do
	    dn=`  $client -vo $vo -file $proxy query-dn  `     || continue
	    voms=`$client -vo $vo -file $proxy query-voms`     || continue
	    life=`$client -vo $vo -file $proxy query-lifetime` || continue
	    srv=` $client -vo $vo -file $proxy query-myproxy-server` ||
		continue

	    log "Renewing $proxy"
	    log "DN: $dn"
	    log "VOMS: $voms"
	    log "Lifetime: $life"
	    log "Server: $srv"

	    $client -vo $vo -file $proxy renew && s=OK || s=FAILED

	    type=`$client -vo $vo -file $proxy query-type` || s=FAILED
	    log "Type: ${type:-unknown}"

	    log "Renewal $s"

	    email=`$client -vo $vo -file $proxy query-email` || continue

	    [ "X$email" = "XN/A" ] && email=

	    proxy_timeleft=`
		$client -vo $vo -file $proxy query-proxy-timeleft
	    ` || continue

	    [ -n "$proxy_timeleft" ] && [ 0 -le "$proxy_timeleft" ] || continue

	    if [ 0 -eq "$proxy_timeleft" ]
	    then
		$client -vo $vo -file $proxy unregister && s=OK || s=FAILED

		log "Proxy expired, unregister $s"

		[ "X$email" = X ] && continue

		host=`hostname -f`
		echo "
		    |This is an automatic mail message from process
		    |$0 running on host $host.
		    |
		    |A proxy certificate for this DN has expired:
		    |$dn
		    |
		    |VOMS arguments:
		    |$voms
		    |
		    |Location:
		    |$proxy
		    |
		" |
		sed 's/[^|]*|//' |
		mail -s "Proxy expiration alert from $host" "$email"

		continue
	    fi

	    [ "X$email" = X ] && continue

	    proxy_thr=`$client -vo $vo -file $proxy query-proxy-safe` ||
		continue

	    if [ "$proxy_timeleft" -le "0$proxy_thr" ]
	    then
		log "Proxy lifetime $proxy_timeleft < $proxy_thr"

		host=`hostname -f`
		echo "
		    |This is an automatic mail message from process
		    |$0 running on host $host.
		    |
		    |A proxy certificate for this DN may soon expire:
		    |$dn
		    |
		    |Time left: $proxy_timeleft
		    |Threshold: $proxy_thr
		    |
		    |VOMS arguments:
		    |$voms
		    |
		    |Location:
		    |$proxy
		    |
		" |
		sed 's/[^|]*|//' |
		mail -s "Proxy expiration alert from $host" "$email"
	    fi

	    myproxy_thr=`$client -vo $vo -file $proxy query-myproxy-safe` ||
		continue

	    myproxy_timeleft=`
		$client -vo $vo -file $proxy query-myproxy-timeleft
	    ` || continue

	    [ -n "$myproxy_timeleft" ] && [ 0 -le "$myproxy_timeleft" ] || continue

	    if [ "$myproxy_timeleft" -le "0$myproxy_thr" ]
	    then
		log "MyProxy lifetime $myproxy_timeleft < $myproxy_thr"

		host=`hostname -f`
		echo "
		    |This is an automatic mail message from process
		    |$0 running on host $host.
		    |
		    |The MyProxy certificate for this DN may soon expire:
		    |$dn
		    |
		    |Time left: $myproxy_timeleft
		    |Threshold: $myproxy_thr
		    |
		" |
		sed 's/[^|]*|//' |
		mail -s "MyProxy expiration alert from $host" "$email"
	    fi
	done

	sleep $timeout
    done
) >> $logdir/events.log 2>&1 < /dev/null &

child=$!

my_xfer $lock $$ $lockdir $child
val=$?

[ $val = 0 ] && exit

echo "$prog: could not transfer lock!" >&2
echo "$prog: killing our instance..."  >&2

kill -9 "$child"

exit $val

