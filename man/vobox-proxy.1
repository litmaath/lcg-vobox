.TH VOBOX-PROXY "1" "Oct 2017" "vobox-proxy 2.1.6" "User Commands"
.SH NAME
vobox-proxy \- manage proxies on a VOBOX for automatic renewal
.SH SYNOPSIS
.B vobox-proxy
[\fIoptions\fR] \fIaction\fR
.br
.SH DESCRIPTION
.PP
\fBvobox-proxy\fR is a tool to manage proxies on a VOBOX for
automatic renewal.
The supported options and actions are detailed below.
.SH OPTIONS
.TP
\fB\-\-classic\fR
Use the classic encoding of the user proxy subject DN plus
the VOMS arguments as the name of the proxy file. This is the default.
See the \fB\-\-md5\fR option for an alternative that leads to much
shorter file names.
.TP
\fB\-\-debug\fR
Print debug information concerning interactions with the MyProxy and
VOMS services.
.TP
\fB\-\-dn \fR{ \fIDN\fR | \fIall\fR }
Specify the proxy subject DN to be used.
By default it is taken from the user's current proxy.
The special value '\fIall\fR' can be used to make certain actions
apply to all currently registered proxies.
.TP
\fB\-\-email \fIaddress\fR
Specify the e-mail address to which to send warnings about the
remaining lifetime of the user's proxy on the VOBOX or on the
MyProxy server.
See options '\fI\-\-myproxy-safe\fR' and '\fI\-\-proxy-safe\fR'.
.TP
\fB\-\-file \fIfilename\fR
Specify the proxy file to be used.
By default it is set to the user's current proxy.
.TP
\fB\-\-force\fR
Needed to allow \fIall\fR proxies to be updated or unregistered
in one go.
.TP
\fB\-h | -\-help\fR
Display a usage reminder and exit.
.TP
\fB\-\-lifetime \fR{ [\fIH\fR]\fIHH:00\fR | \fIseconds\fR }
Specify the desired proxy lifetime either in hours or seconds
as indicated. Currently only an integral number of hours is
supported, any remaining minutes are ignored. The default is 12 hours.
See the '\fI\-t\fR' option for important additional information.
.TP
\fB\-\-md5\fR
Use a 32-byte hexadecimal MD5 encoding of the user proxy subject DN plus
the VOMS arguments as the name of the proxy file, instead of the classic
encoding that typically leads to very long file names.  The MD5 encoding
is enforced when the classic encoding would lead to a length exceeding the
POSIX limit of 255 characters.  MD5 will then be used only for encoding
the VOMS arguments if that is already sufficient to get below the limit.
.TP
\fB\-\-myproxy\-safe \fR{ [\fIH\fR]\fIHH:MM\fR | \fIseconds\fR }
Specify the lifetime safety threshold of the proxy stored on the
MyProxy server: if its remaining lifetime falls below the given value,
a warning e-mail is sent to the address specified with the '\fI\-\-email\fR'
option. The value can be specified either in hours
and minutes or in seconds as indicated. The default value is zero,
which will cause a warning e-mail to be sent when the proxy has
expired, if an e-mail address was supplied.
.TP
\fB\-\-proxy\-safe \fR{ [\fIH\fR]\fIHH:MM\fR | \fIseconds\fR }
Specify the lifetime safety threshold of the proxy stored on the
VOBOX: if its remaining lifetime falls below the given value,
a warning e-mail is sent to the address specified with the '\fI\-\-email\fR'
option. The value can be specified either in hours
and minutes or in seconds as indicated. The default value is zero,
which will cause a warning e-mail to be sent when the proxy has
expired, if an e-mail address was supplied.
.TP
\fB\-\-root \fIdirectory\fR
Specify the root of the VOBOX proxy management directory hierarchy.
The default value is '\fI/var/lib/vobox\fR'.
.TP
\fB\-s \fIMyProxy_server\fR
Specify the MyProxy server to be used, by default the one specified
in the VO-specific configuration file, if any, else the one specified
in '\fI/etc/vobox/vobox-proxy.conf\fR', if present, else the value of
the \fBMYPROXY_SERVER\fR environment variable.
.TP
\fB\-t \fR[\fIH\fR]\fIHH\fR
Specify the desired proxy lifetime in hours. The same option and
argument need to be supplied to the '\fImyproxy\-init\fR' command
when the long-lived proxy is uploaded onto the MyProxy server.
The default is 12 hours.
.TP
\fB\-V | \-\-version\fR
Print version number and exit.
.TP
\fB\-\-vo \fIVO\fR
Specify the VO to which the given action applies.
By default it is taken from the user's current proxy.
.TP
\fB\-\-voms \fIFQAN\fR
Specify the VOMS FQAN (fully qualified attribute name) to be used.
The format is the same as used in the '\fIvoms\-proxy\-init\fR' command.
This option can be repeated when multiple concurrent roles need to
be taken into account.
By default it is set to the primary FQAN in the user's current proxy.
.SH CONFIGURATION
A host-wide configuration file '\fI/etc/vobox/vobox-proxy.conf\fR'
is consulted if present, containing definitions as explained below.
Each VO can have its own configuration file whose path is given by
\fB$VOBOX_PROXY_ROOT\fI/\fB$vo\fI/etc/vobox-proxy.conf\fR where
\fB$VOBOX_PROXY_ROOT\fR is \fI/var/lib/vobox\fR by default and \fB$vo\fR
indicates the VO name in lower case. The definitions in this file have
priority over the host-wide configuration and may themselves be overridden
by command-line options.
Each of the lines in either file may redefine the default
value of one of the following parameters, according to the given syntax:
.sp
\fBHOURS_DEF = \fIproxy lifetime default number of hours\fR
.sp
\fBMYPROXY_SERVER = \fIMyProxy server to be used\fR
.sp
\fBUSE_MD5 = \fI0 (default) or 1\fR
.sp
Unexpected parameters are ignored, while unacceptable values are skipped
with a warning.  A '\fI#\fR' character introduces a comment extending
to the end of its line.
.SH ACTIONS
.PP
The special value '\fIall\fR' for the DN can be used with any of
the '\fIquery\fR', '\fIquery\-*\fR', '\fIrenew\fR', '\fIunregister\fR'
and '\fIupdate\fR' actions.
.TP
\fBquery\fR
Print the configuration parameters and status for the matching
registered proxies.
.TP
\fBquery\-dn\fR
Print the subject DN for the matching registered proxies.
.TP
\fBquery\-email\fR
Print the e-mail address for the matching registered proxies.
.TP
\fBquery\-lifetime\fR
Print the lifetime requested for the matching registered proxies.
.TP
\fBquery\-myproxy\-safe\fR
Print the MyProxy lifetime safety threshold for the matching
registered proxies.
.TP
\fBquery\-myproxy\-server\fR
Print the MyProxy server for the matching registered proxies.
.TP
\fBquery\-myproxy\-timeleft\fR
Print the remaining MyProxy lifetime for the matching registered
proxies.
.TP
\fBquery\-proxy\-filename\fR
Print the proxy file name for the matching registered proxies.
.TP
\fBquery\-proxy\-safe\fR
Print the proxy lifetime safety threshold for the matching
registered proxies.
.TP
\fBquery\-proxy\-timeleft\fR
Print the remaining proxy lifetime for the matching registered
proxies.
.TP
\fBquery\-type\fR
Print the type ('\fIlegacy\fR' or '\fIrfc\fR') of the matching
registered proxies.
.TP
\fBquery\-voms\fR
Print the VOMS attributes requested for the matching registered
proxies.
.TP
\fBregister\fR
Register a proxy for renewal. A previously registered proxy with
the same DN and VOMS attributes is automatically replaced.
.TP
\fBrenew\fR
Renew the matching registered proxies.
This action is regularly invoked by the daemon that manages the
repository of registered proxies.
.TP
\fBunregister\fR
Unregister the matching proxies.
.TP
\fBupdate\fR
Update configuration parameters for the matching registered proxies.
The parameters that can be updated are the e-mail address,
the requested lifetime and the safety
thresholds for the proxy and MyProxy lifetimes.
.SH AUTHOR
Versions <= 1.0.1 by Simone Campana
.br
Versions -> 1.0.4 by Elisa Lanciotti
.br
Versions -> 1.0.5 by Andrew Elwell
.br
Versions >= 2.0.0 by Maarten Litmaath
.br
.SH "REPORTING BUGS"
Report bugs via GGUS (http://ggus.org).
.SH "SEE ALSO"
\fBvobox-renewd\fR(8), \fBvoname-box-proxyrenewal\fR(8)
